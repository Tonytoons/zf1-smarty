

__________               __      _________ __                
\______   \ ____   ____ |  | __ /   _____//  |______ _______ 
 |       _//  _ \_/ ___\|  |/ / \_____  \\   __\__  \\_  __ \
 |    |   (  <_> )  \___|    <  /        \|  |  / __ \|  | \/
 |____|_  /\____/ \___  >__|_ \/_______  /|__| (____  /__|   
        \/            \/     \/        \/           \/       
    ----------------------------------------------------------------- 

Hi there! Welcome to easy PHP framework for beginners!

Good for beginner who have no idea about PHP framework, let start from easy code here.

To get you started, let download all files and test run with Apache or try on Clound9(c9.io).

1) Start from ./applications/config.xml

2) For serious configuration, you have to change DB configuration from here <database>

3) Make sure your DB is correct and have "users" table and contain field id, name and last_update

Ex1)
Start from IndexController ./applications/controllers/IndexController.php

How to display to "Smarty View", lets code like below.
$this->_smarty->display('web/index.tpl');

Check ./themes/default/templates/web/
They are using .tpl

Ex2)
How to use multiple languages
Check ./languages/
All language keys are in text.txt like below.
hello = Hello

URL will be like this https://www.xxxx.com/[Controller]/[Action]/lang/[language]/ :: https://www.xxxx.com/index/index/lang/th/
For router you can change later, but for the beginner let follow this structure first.

Ex3)
Check another Action on IndexController(How to add data - edit - view - delete)

Happy coding!
SCG CTO,
Tonytoons


## Documentation

ZF1 https://framework.zend.com/manual/1.12/en/learning.quickstart.html
Smarty https://www.smarty.net/
Bootstrap http://getbootstrap.com/getting-started/ for css framework!

git clone git@bitbucket.org:Tonytoons/zf1-smarty.git


#Upgrade to php7 on C9. #follow below.

sudo add-apt-repository ppa:ondrej/php -y

sudo apt-get update -y

sudo apt-get install php7.0-curl php7.0-cli php7.0-dev php7.0-gd php7.0-intl php7.0-mcrypt php7.0-json php7.0-mysql php7.0-opcache php7.0-bcmath php7.0-mbstring php7.0-soap php7.0-xml php7.0-zip -y

sudo mv /etc/apache2/envvars /etc/apache2/envvars.bak

sudo apt-get remove libapache2-mod-php5 -y

sudo apt-get install libapache2-mod-php7.0 -y

sudo cp /etc/apache2/envvars.bak /etc/apache2/envvars

sudo apt-get purge php*

sudo apt-get install php7.0-mysql php7.0-curl php7.0-json php7.0-cgi  php7.0 libapache2-mod-php7.0

sudo apt-get install php7.0-mbstring

sudo apt-get autoremove

sudo service apache2 restart

sudo apt-get install php7.0-simplexml

sudo service apache2 restart

