<?php
 class IndexController extends MyBaseControllerAction
 {
##########################################################################
	function indexAction()
 	{
    	try
        {
			$this->_smarty->display('web/index.tpl');
       	}
       	catch (Zend_Exception $e)
    	{
       		$this->_smarty->display('Underconstruction/index.tpl');
    	}
	}
##########################################################################
	function userAction()
 	{	
    	try
        {
            $this->act = $this->_getParam('act');
            $id = $this->_getParam('id');
            Zend_Loader::loadClass('User');
			$users = new User($id);
            if($this->act == 'add')
            {
                $name = $this->_getParam('name');
                if($name)
                {
                    $users->add($name);
                    $this->_response->setRedirect($this->_request->getBaseUrl() . '/index/user/');
                }
            }
            else if($this->act == 'detail')
            {
                $data = $users->getList();
                $this->_smarty->assign('data', $data);
                if($id)
                {
                    $detail = $users->getDetail();
                    $this->_smarty->assign('detail', $detail);
                }
                else
                {
                    $this->_response->setRedirect($this->_request->getBaseUrl() . '/index/user/');
                }
            }
            else if($this->act == 'edit')
            {
                $name = $this->_getParam('name');
                if($name)
                {
                    $users->edit($name);
                    $this->_response->setRedirect($this->_request->getBaseUrl() . '/index/user/');
                }
            }
            else if($this->act == 'del')
            {
                $users->del();
                $this->_response->setRedirect($this->_request->getBaseUrl() . '/index/user/');
            }
            else
            {
                $data = $users->getList();
                $this->_smarty->assign('data', $data);
            }
			$this->_smarty->display('web/user.tpl');
       	}
       	catch (Zend_Exception $e)
    	{ print_r($e);
       		$this->_smarty->display('Underconstruction/index.tpl');					
    	}
	}
#################################################################
 }
?>