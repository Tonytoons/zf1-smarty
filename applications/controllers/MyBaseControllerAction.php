<?php
class MyBaseControllerAction extends Zend_Controller_Action
{
    protected $_config;
    protected $_smarty;
    protected $_lang;
    protected $_db;
	
    /**
    *   Initial method
    */
    public function init()
    {
        // load configuration
		$this->page = $this->_getParam('page');
        if((!(isset($this->page))) or ($this->page == '')) $this->page = '1';
        
		$this->_config = new Zend_Config_Xml('Tonytoons', 'general');
		//DB
		$this->db_app = Zend_Db::factory($this->_config->database->adapter, $this->_config->database->toArray());
        $this->db_app->getConnection()->exec("SET NAMES UTF8");//UTF8
        $this->db_app->setFetchMode(Zend_Db::FETCH_ASSOC);
        Zend_Registry::Set('db_app',$this->db_app);
#########################################
        // pre-define smarty object
        $smarty = new Smarty;
        $smarty->debugging      = false;
        $smarty->force_compile  = true;
        $smarty->caching        = false;
        $smarty->compile_check  = true;
        $smarty->cache_lifetime = -1;
        $smarty->template_dir   = DOCUMENT_ROOT . 'themes/' . $this->_config->defaultTheme . '/templates';
        $smarty->compile_dir    = DOCUMENT_ROOT . 'themes/' . $this->_config->defaultTheme . '/templates_c';
        $smarty->config_dir     = DOCUMENT_ROOT . 'languages/';
        $smarty->plugins_dir    = array(SMARTY_DIR . 'plugins', './libs/SmartyPlugins');
        
        $this->_smarty =& $smarty;
        $this->lang = $this->_getParam('lang');
		if( (empty($this->lang)) || ($this->lang == '') ) $this->lang = $this->_config->defaultLanguage;
		
		$this->_smarty->assign('page', $this->page);
        $this->_smarty->assign('basePath', $this->_request->getBasePath());
		
        $reqParams = $this->_request->getParams();
        if( $reqParams ) foreach( $reqParams as $k => $v)
        {
            $this->_smarty->assign($k, $v);
        }
		
		$this->now = date("Y-m-d H:i:s");
		$this->_smarty->assign('lang', $this->lang);
		$this->_smarty->assign('now', $this->now);
    }
}
?>