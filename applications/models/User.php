<?php
    class User
    {

		protected $users;
################################################################
		public function __construct($inID)
		{
			$this->id = $inID;
			$this->_config = new Zend_Config_Xml(DOCUMENT_ROOT . "config.xml", "general");
			$this->now = date("Y-m-d H:i:s");
			$this->cacheTime = $this->_config->cacheTime;
			$this->server_host = $_SERVER['HTTP_HOST'];
		}
################################################################
        public function getList()
        {
			$this->_db = Zend_Registry::Get('db_app');
			if($search != '%') $q1 = '';
            $sql = "SELECT id, name, last_update FROM users ORDER BY last_update DESC";
			$result = $this->_db->fetchAll($sql);
			foreach($result as $result=>$item)
			{
				$data[$result] = array(
					                   		'id' => $item['id'],
					                   		'name' => $item['name'],
					                   		'last_update' => $item['last_update'],
                                   		);										  
			}
			if(isset($data)) return ($data);
		}
################################################################
        public function getNextId()
        {
			$this->_db = Zend_Registry::Get('db_app');
			$sql    = "SELECT MAX(id) + 1 AS id FROM users LIMIT 1";
       		$result = $this->_db->fetchRow($sql);
			$id     = $result['id'];
			if($id == NULL) $id = '1';
			return ( $id );
		}
################################################################
        public function getDetail()
        {
			$this->_db = Zend_Registry::Get('db_app');
            $sql = "SELECT name, last_update FROM users WHERE id = '$this->id' LIMIT 1";
			$result = $this->_db->fetchRow($sql);
			$data = array(
								'id' => $this->id,
								'name' => $result['name'],
								'last_update' => $result['last_update']
							);
			return ($data);
		}
################################################################
        public function add($name)
        {
        	$id = $this->getNextId();
			$this->_db = Zend_Registry::Get('db_app');
			$data_new  = array(
									'id' => $id,
									'name' => $name,
									'last_update' => $this->now
            					);
			$this->_db->insert('users', $data_new);
		}
################################################################
        public function edit($name)
        {
			$this->_db = Zend_Registry::Get('db_app');
			$data_update  = array(
                                 	'name' => $name,
									'last_update' => $this->now
            					);
			$where[] = "id = '$this->id'";
			$this->_db->update('users', $data_update, $where);
		}
################################################################
        public function del()
        {
			$this->_db = Zend_Registry::Get('db_app');
			$this->_db->delete('users', "id  = '$this->id' ");
		}
################################################################
	}
?>