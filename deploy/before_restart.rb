directory "#{release_path}/themes/default/templates_c" do
  owner "www-data"
  group "www-data"
  mode "0777"
  action :create
  not_if do ::File.exists?("#{release_path}/themes/default/templates_c") end
end

directory "#{release_path}/cache" do
  owner "www-data"
  group "www-data"
  mode "0777"
  action :create
  not_if do ::File.exists?("#{release_path}/cache") end
end

directory "#{release_path}/cache/admin" do
  owner "www-data"
  group "www-data"
  mode "0777"
  action :create
  not_if do ::File.exists?("#{release_path}/cache/admin") end
end

directory "#{release_path}/cache/admin/dashboard" do
  owner "www-data"
  group "www-data"
  mode "0777"
  action :create
  not_if do ::File.exists?("#{release_path}/cache/admin/dashboard") end
end

directory "#{release_path}/cache/admin/referralCode" do
  owner "www-data"
  group "www-data"
  mode "0777"
  action :create
  not_if do ::File.exists?("#{release_path}/cache/admin/referralCode") end
end

execute "autoremove" do
  command "apt-get autoremove mod_php5_apache2"
  action :run
end

execute "repository" do
  command "add-apt-repository ppa:ondrej/php -y"
  action :run
end

execute "update" do
  command "apt-get update -y"
  action :run
end

execute "install" do
  command "apt-get --yes --force-yes install php7.0-curl php7.0-cli php7.0-dev php7.0-gd php7.0-intl php7.0-mcrypt php7.0-json php7.0-mysql php7.0-opcache php7.0-bcmath php7.0-mbstring php7.0-soap php7.0-xml php7.0-zip -y"
  action :run
end

execute "envvars" do
  command "mv /etc/apache2/envvars /etc/apache2/envvars.bak"
  action :run
end

execute "remove" do
  command "apt-get remove libapache2-mod-php5 -y"
  action :run
end

execute "install" do
  command "apt-get --force-yes install libapache2-mod-php7.0 -y"
  action :run
end

execute "envvars" do
  command "cp /etc/apache2/envvars.bak /etc/apache2/envvars"
  action :run
end

execute "purge" do
  command "apt-get --yes --force-yes purge php*"
  action :run
end

execute "install" do
  command "apt-get --yes --force-yes install php7.0-mysql php7.0-curl php7.0-json php7.0-cgi  php7.0 libapache2-mod-php7.0 php7.0-mbstring php7.0-simplexml"
  action :run
end

execute "apache2" do
  command "service apache2 restart"
  action :run
end
