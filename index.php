<?php
try
{
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: text/html; charset=utf-8");
	
 	error_reporting(E_ALL|E_STRICT);
 	ini_set('display_errors','0');
	date_default_timezone_set('Asia/Bangkok');
 	mb_internal_encoding("UTF-8");
 	$docsRoot = getenv("DOCUMENT_ROOT");
 	$docsRoot = @str_replace('\/$', '', $docsRoot) ."/";
 
 	define('DOCUMENT_ROOT', $docsRoot);
 
	 set_include_path(DOCUMENT_ROOT
    				. PATH_SEPARATOR . DOCUMENT_ROOT . 'libs/'
    				. PATH_SEPARATOR . DOCUMENT_ROOT . 'libs/Business/'
    				. PATH_SEPARATOR . DOCUMENT_ROOT . 'libs/Smarty/'
    				. PATH_SEPARATOR . DOCUMENT_ROOT . 'applications/models/'
    				. PATH_SEPARATOR . DOCUMENT_ROOT . 'applications/controllers/'
    				. PATH_SEPARATOR . get_include_path()
                  );

 	require "Smarty.class.php";
 	require_once 'Zend/Loader.php'; // the Zend dir must be in your include_path
 	require_once 'Zend/Controller/Action.php';
 	Zend_Loader::loadClass('Zend_Cache');
 	Zend_Loader::loadClass('Zend_Controller_Front');
 	Zend_Loader::loadClass('MyBaseControllerAction');
 	Zend_Loader::loadClass('Zend_Config_Xml');
 	Zend_Loader::loadClass('Zend_Registry');
 	Zend_Loader::loadClass('Zend_Db');
 	Zend_Loader::loadClass('Zend_Session');
 	Zend_Loader::loadClass('Zend_Session_Namespace');
 	Zend_Loader::loadClass('Zend_Mail');
 	Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
	Zend_Loader::loadClass('Zend_Json'); 
	Zend_Loader::loadClass('Zend_Http_Cookie'); //boy set Cookie
	Zend_Loader::loadClass('Zend_Service_Amazon_S3');
	Zend_Loader::loadClass('Zend_Pdf');
 	Zend_Session::start();

 	// setup controller
 	$front = Zend_Controller_Front::getInstance();
 	$front->throwExceptions(true);
	$front->setParam('noViewRenderer', true);
 	$front->setControllerDirectory(DOCUMENT_ROOT . "applications/controllers");
 	$front->dispatch();
}
catch (Zend_Exception $e)
{ //print_r($e);
	 header( 'Location:https://zf1-smarty-tonytoons.c9users.io/error404.html' );
}								  
?>
