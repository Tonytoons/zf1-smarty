<!DOCTYPE html>
{if $controller}{config_load file="$lang/text.txt" section=$controller}{else}{config_load file="th/text.txt"}{/if}
<html> 
    <head>
        <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=UTF-8">
        <meta http-equiv="Cache-control" content="public">
        <META HTTP-EQUIV="EXPIRES" CONTENT="3600">
        <meta http-equiv="cleartype" content="on">
        <meta name="MobileOptimized" content="767">
        <meta name="HandheldFriendly" content="True">
        <meta name='mobile-web-app-capable' content='yes'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = no">
        <link rel="shortcut icon" href="{$basePath}/themes/default/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="{$basePath}/themes/default/images/favicon.ico" type="image/x-icon">
        <link rel="icon" type="image/png" sizes="32x32" href="{$basePath}/themes/default/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{$basePath}/themes/default/images/favicon.ico">
        <link rel="icon" type="image/png" sizes="96x96" href="{$basePath}/themes/default/images/favicon.ico">
        <link rel="icon" type="image/png" sizes="16x16" href="{$basePath}/themes/default/images/favicon.ico">
        <link rel="manifest" href="{$basePath}/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">
        <meta name='application-name' content='Attraction'>
	    <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name='apple-mobile-web-app-status-bar-style' content='black'>
        <meta name='apple-mobile-web-app-title' content='Attraction'>
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <META NAME="AUTHOR" CONTENT="NRRU">
        <META name="revisit-after" content="7 days">
        <META NAME="COPYRIGHT" CONTENT="&copy; 2017 NRRU">
        <title>{#nrru#}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="{$basePath}/themes/default/css/style.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container" style="margin-top:10px;">
            <div class="jumbotron">
                <h1>{#hello#}, ZF1 & Smarty!</h1>
                <p>...</p>
                <p><a class="btn btn-primary btn-lg" href="{$basePath}/index/user" role="button">{#learn_more#}</a></p>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" defer></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.min.js" defer></script>
        <script src="{$basePath}/themes/default/js/myJS.js" defer></script>
    </body>
</html>