<?php
/* Smarty version 3.1.30, created on 2017-08-08 10:34:32
  from "/home/ubuntu/workspace/themes/default/templates/admin/footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59893148c02d16_40864666',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0052cd930545b51d38ed9896aabbf8948e5a0584' => 
    array (
      0 => '/home/ubuntu/workspace/themes/default/templates/admin/footer.tpl',
      1 => 1502122108,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59893148c02d16_40864666 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['action']->value != 'index') {?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1
        </div>
        <strong>Copyright &copy; 2015 - <?php echo date('Y');?>
, <a href="https://admin.fabbrigade.com"> ADMIN FABbrigade</a>.</strong> All rights reserved.
    </footer>
<?php }?>
</body>
</html><?php }
}
