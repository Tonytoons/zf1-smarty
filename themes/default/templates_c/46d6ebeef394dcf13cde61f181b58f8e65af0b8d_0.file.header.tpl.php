<?php
/* Smarty version 3.1.30, created on 2017-08-08 10:34:32
  from "/home/ubuntu/workspace/themes/default/templates/admin/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59893148bdf150_92530670',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '46d6ebeef394dcf13cde61f181b58f8e65af0b8d' => 
    array (
      0 => '/home/ubuntu/workspace/themes/default/templates/admin/header.tpl',
      1 => 1502122109,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59893148bdf150_92530670 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<?php if ($_smarty_tpl->tpl_vars['controller']->value) {
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['lang']->value)."/admin.txt", $_smarty_tpl->tpl_vars['controller']->value, 0);
} else {
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, "th/admin.txt", null, 0);
}?>
<html>
  	<head>
  	    <meta http-equiv="cleartype" content="on">
        <meta name="MobileOptimized" content="767">
        <meta name="HandheldFriendly" content="True">
        <meta name='mobile-web-app-capable' content='yes'>
        <link rel="icon" sizes="192x192" href="https://files.fabbrigade.com/icon/admin-icon.png">
        <link rel="apple-touch-icon" href="https://files.fabbrigade.com/icon/admin-icon.png"> 
    	<link rel="apple-touch-icon" sizes="152x152" href="https://files.fabbrigade.com/icon/admin-icon.png">
    	<link rel="apple-touch-icon" sizes="180x180" href="https://files.fabbrigade.com/icon/admin-icon.png">
    	<link rel="apple-touch-icon" sizes="167x167" href="https://files.fabbrigade.com/icon/admin-icon.png">
    	<meta name='application-name' content='FABadmin'>
    	<meta name="theme-color" content="#f2e3e0">
    	<meta name="apple-mobile-web-app-capable" content="yes">
        <meta name='apple-mobile-web-app-status-bar-style' content='black'>
        <meta name='apple-mobile-web-app-title' content='FABadmin'>
        <link rel="apple-touch-startup-image" href="https://files.fabbrigade.com/web-v2.0/screenshots.jpg">
  	    <link rel='manifest' href='<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/manifest.json'>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php if ($_smarty_tpl->tpl_vars['action']->value == 'index') {?>
    		<title><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'home_title');?>
</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'category') {?>
        	<title>Category</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'country') {?>
        	<title>Country</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'admin') {?>
        	<title>Admin users</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'city') {?>
        	<title>City</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'booking') {?>
        	<title>Booking</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'fabsquadpayout') {?>
        	<title>FABsquad payout</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'salesreport') {?>
        	<title>Sales Report</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'subcategory') {?>
        	<title>Sub-category</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'service') {?>
        	<title>Services</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'user') {?>
            <?php if (($_smarty_tpl->tpl_vars['act']->value == 'referralCode') || ($_smarty_tpl->tpl_vars['act']->value == 'editRFCform')) {?>
                <title>Referral Code</title>
            <?php } else { ?>
        	    <title>Customer</title>
        	<?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'promocodev1') {?>
        	<title>Promo code v1</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'fabsquad') {?>
        	<title>FABsquad</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'mailChimpList') {?>
        	<title>mailChimp</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'press') {?>
        	<title>Press</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'homepopup') {?>
        	<title>Home popup</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'language') {?>
        	<title>Language</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'thirdpartycode') {?>
        	<title>3rd Party Code</title>
        <?php } elseif ($_smarty_tpl->tpl_vars['action']->value == 'fabsquadcode') {?>
        	<title>FABsquad Code</title>
        <?php } else { ?>
        	<title>Dashboard</title>
        <?php }?>
    	<!-- Tell the browser to be responsive to screen width -->
    	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    	<!-- Bootstrap 3.3.5 -->
    	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/bootstrap/css/bootstrap.min.css">
    	<!-- Font Awesome -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    	<!-- Ionicons -->
    	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    	<!-- Theme style -->
    	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/dist/css/AdminLTE.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/dist/css/skins/_all-skins.min.css">
        <!-- Bootstrap time Picker -->
    	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/timepicker/bootstrap-timepicker.min.css">
    	<!-- iCheck -->
    	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/iCheck/square/blue.css">
        <!-- daterange picker -->
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/css/admin.css">
		<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico">
  		<link rel="icon" type="image/png" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon-32x32.png">
    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
        	<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
        	<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    	<![endif]-->
        <!-- jQuery 2.1.4 -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/jQuery/jQuery-2.1.4.min.js"><?php echo '</script'; ?>
>
    	<!-- Bootstrap 3.3.5 -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    	<!-- iCheck -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
        <!-- Select2 -->
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/select2/select2.full.min.js"><?php echo '</script'; ?>
>
        <!-- InputMask -->
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/input-mask/jquery.inputmask.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/input-mask/jquery.inputmask.extensions.js"><?php echo '</script'; ?>
>
        <!-- date-range-picker -->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>
        <!-- bootstrap color picker -->
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"><?php echo '</script'; ?>
>
        <!-- bootstrap time picker -->
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/timepicker/bootstrap-timepicker.min.js"><?php echo '</script'; ?>
>
    	<!-- FastClick -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/fastclick/fastclick.min.js"><?php echo '</script'; ?>
>
    	<!-- AdminLTE App -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/dist/js/app.min.js"><?php echo '</script'; ?>
>
    	<!-- Sparkline -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/sparkline/jquery.sparkline.min.js"><?php echo '</script'; ?>
>
    	<!-- jvectormap -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"><?php echo '</script'; ?>
>
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"><?php echo '</script'; ?>
>
    	<!-- SlimScroll 1.3.0 -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/slimScroll/jquery.slimscroll.min.js"><?php echo '</script'; ?>
>
    	<!-- ChartJS 1.0.1 -->
    	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/plugins/chartjs/Chart.min.js"><?php echo '</script'; ?>
>
    	<!-- CK Editor -->
        <?php echo '<script'; ?>
 src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"><?php echo '</script'; ?>
>
    	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    	<!-- AdminLTE for demo purposes -->
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/templates/admin/dist/js/demo.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
>
			var basePath = '<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
';
			var cityID = '<?php echo $_smarty_tpl->tpl_vars['cityID']->value;?>
';
		<?php echo '</script'; ?>
>
		<?php if ($_smarty_tpl->tpl_vars['action']->value == 'booking') {?>
		    <?php if ($_smarty_tpl->tpl_vars['act']->value == 'addForm') {?>
    		    <?php echo '<script'; ?>
 type="text/javascript">
             	    var api_url = '<?php echo $_smarty_tpl->tpl_vars['api_url']->value;?>
un/api/pa/cd148fe9bafd6874cd411a0309bb7cc3/key/TonyWilliamKong/type/';
             	    var id = '<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
';
             	    var date = '<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
';
             	    var time = '<?php echo $_smarty_tpl->tpl_vars['time']->value;?>
';
             	    var phone = '<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
';
             	    var lat = '<?php echo $_smarty_tpl->tpl_vars['lat']->value;?>
';
             	    var lon = '<?php echo $_smarty_tpl->tpl_vars['lon']->value;?>
';
             	    var lang = '<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
';
             	    var omesiCoxCard = '<?php echo $_smarty_tpl->tpl_vars['omesiCoxCard']->value;?>
';
                    var address = "<?php echo $_smarty_tpl->tpl_vars['address']->value;?>
";
             	<?php echo '</script'; ?>
>
         	<?php }?>
        <?php }?>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/js/admin.js?20170109-1"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });
        <?php echo '</script'; ?>
>
  	</head><?php }
}
