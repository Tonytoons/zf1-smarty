<?php
/* Smarty version 3.1.30, created on 2017-08-08 15:46:56
  from "/home/ubuntu/workspace/themes/default/templates/web/hello.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59897a80e27eb9_46170027',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af12d05c0c8591c3a5e8a13a49b5ebeeeaf1539a' => 
    array (
      0 => '/home/ubuntu/workspace/themes/default/templates/web/hello.tpl',
      1 => 1502181974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59897a80e27eb9_46170027 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<?php if ($_smarty_tpl->tpl_vars['controller']->value) {
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['lang']->value)."/text.txt", $_smarty_tpl->tpl_vars['controller']->value, 0);
} else {
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, "th/text.txt", null, 0);
}?>
<html> 
    <head>
        <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=UTF-8">
        <meta http-equiv="Cache-control" content="public">
        <META HTTP-EQUIV="EXPIRES" CONTENT="3600">
        <meta http-equiv="cleartype" content="on">
        <meta name="MobileOptimized" content="767">
        <meta name="HandheldFriendly" content="True">
        <meta name='mobile-web-app-capable' content='yes'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = no">
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico" type="image/x-icon">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/images/favicon.ico">
        <link rel="manifest" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">
        <meta name='application-name' content='Attraction'>
	    <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name='apple-mobile-web-app-status-bar-style' content='black'>
        <meta name='apple-mobile-web-app-title' content='Attraction'>
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <META NAME="AUTHOR" CONTENT="NRRU">
        <META name="revisit-after" content="7 days">
        <META NAME="COPYRIGHT" CONTENT="&copy; 2017 NRRU">
        <title><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'nrru');?>
</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/css/style.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
        <![endif]-->
    </head>
    <body>
        <div class="container"style="margin-top:10px;">
            <div class="jumbotron">
                <h1><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'hello');?>
, NRRU!</h1>
                <p>...</p>
                <p><a class="btn btn-primary btn-lg" href="javascript:doNothing();" role="button"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'learn_more');?>
</a></p>
            </div>
        </div>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" defer><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" defer><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.min.js" defer><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/themes/default/js/myJS.js" defer><?php echo '</script'; ?>
>
    </body>
</html><?php }
}
