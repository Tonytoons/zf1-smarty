<?php
/* Smarty version 3.1.30, created on 2017-08-08 10:34:32
  from "/home/ubuntu/workspace/themes/default/templates/admin/login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59893148b01dc8_96373691',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c1393448ae5bfa0808a362af6cbdc04d50eed270' => 
    array (
      0 => '/home/ubuntu/workspace/themes/default/templates/admin/login.tpl',
      1 => 1502122109,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/header.tpl' => 1,
    'file:admin/footer.tpl' => 1,
  ),
),false)) {
function content_59893148b01dc8_96373691 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body class="hold-transition login-page" style="background:#f2e3e0;">
<div class="login-box">
	<div class="login-logo">
  		<a href="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/"><b>Admin</b> Fabbrigade</a>
  	</div>
  	<div class="login-box-body">
  		<p class="login-box-msg">Sign in to start your session</p>
        <form action="<?php echo $_smarty_tpl->tpl_vars['basePath']->value;?>
/index/login/" method="post" onSubmit="return checkLogin()" name="quest">
     		<div class="form-group has-feedback">
            	<input type="email" class="form-control" id="email" name="email" placeholder="Email" style="border-color:#7c7d80;">
            	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          	</div>
          	<div class="form-group has-feedback">
            	<input type="password" class="form-control" id="password" name="password" placeholder="Password" style="border-color:#7c7d80;">
            	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
          	</div>
          	<div class="row">
          		<div class="col-xs-8"></div>
                
            	<div class="col-xs-4">
              		<button type="submit" class="btn btn-primary btn-block btn-flat" style="background:#7c7d80; border-color:#7c7d80;">
              		    Sign In
              	    </button>
            	</div>
          	</div>
        </form>
 	</div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
